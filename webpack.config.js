module.exports = {
    entry: {
        app: './app/frontend/app.jsx'
    },
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
        ]
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/app/backend/static'
    }
}
