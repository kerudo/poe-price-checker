import requests
import json
import argparse
import time
import re
from pprint import pprint
from pyparsing import *
from items import *

def getTab (tabIndex, acct, sessid):
    url = "https://pathofexile.com/character-window/get-stash-items?league=Harbinger&tabs=1&tabIndex=" + str(tabIndex) + "&accountName=" + acct
    headers = {'Cookie': 'POESESSID=' + sessid}

    return json.loads(requests.get(url, headers=headers).text)

def getTabIndexByName (tabName, acct, sessid):
    tabs = getTabList(acct, sessid)
    for tab in tabs:
        if tab['n'] == tabName:
            print ("found tab")
            return i

def getTabList (acct, sessid):
    response = getTab(0, acct, sessid)
    return response['tabs']

def parseTab (tabIndex, acct, sessid):
    response = getTab(tabIndex, acct, sessid)
    items = response['items']

    parsedItems = []

    with open('itemdata.json', 'r') as itemDataFile:
        itemData = json.loads(itemDataFile.read())
        for item in items:
            # filter out uniques and stuff without explicits
            if "flavourText" not in item and "explicitMods" in item:
                # pull the item type out of the thumbnail url (why only here!?)
                urlList = item['icon'].split('/')
                if urlList[6] == "Weapons":
                    if urlList[7] == "OneHandWeapons":
                        itemType = "OneHand" + urlList[8]
                    elif urlList[7] == "TwoHandWeapons":
                        itemType = "TwoHand" + urlList[8]
                else:
                    itemType = urlList[6]
                    if itemType in itemData.keys():
                        # Create an class instance of the appropriate item
                        newItem = Item(item, itemType, itemData)
                        newItem.eval_value()
                        print ('\n' + newItem.name)
                        print (newItem.itemType)
                        for mod in newItem.evaluations[0]['scoredMods']:
                            if mod[1] > 0:
                                print ("%s is %i%% perfect" % (mod[0], mod[1]))
                            else:
                                print ("%s is a dud. %s pts." % (mod[0], mod[1]))
                        print ("TOTAL SCORE: %i" % newItem.evaluations[0]['totalScore'])
                        parsedItems.append(newItem)
