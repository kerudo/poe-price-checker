import requests
import json
import argparse
import time
import re
from pprint import pprint
from pyparsing import *

def parse (modString):
    # Basics
    plus = Literal("+").suppress()
    minus = Literal("-").suppress()
    percent = Literal("%").suppress()
    to = CaselessLiteral("To").suppress()
    adds = CaselessLiteral("Adds").suppress()
    prefix = CaselessLiteral("Reflects") | CaselessLiteral("Minions Have") | CaselessLiteral("Totems Gain")
    prop = Word(alphas + " ")

    # properties
    basicProperty = Optional(prefix('prefix')) + Optional(plus) + Optional(Word(nums)("value")) + Optional(percent) + Optional(to) + prop("property")
    damageProperty = adds + Word(nums)("valLow") + to + Word(nums)("valHigh") + prop("property")

    propParser = damageProperty | basicProperty

    try:
        parseResult =  propParser.parseString(modString)
    except ParseException:
        print ("Parsing failed for property: '%s'" % modString)
        parseResult = ''

    if 'valLow' in parseResult:
        avg = (float(parseResult['valLow']) + float(parseResult['valHigh'])) / 2
        parseResult = [avg, parseResult[-1]]

    if 'prefix' in parseResult:
        parseResult = [parseResult['value'], parseResult[2] + ' (' + parseResult[0].split(' ')[0] + ")"]

    return parseResult
    # print (result)

class Item(object):
    def __init__(self, itemJSON, itemType, itemData):
        self.w = itemJSON['w']
        self.h = itemJSON['h']
        self.icon = itemJSON['icon']
        self.itemID = itemJSON['icon']
        self.name = re.sub(r'<.+>', '', itemJSON['name'])
        self.typeLine = itemJSON.get('typeLine', "")
        self.properties = itemJSON.get('properties', None)
        self.x = itemJSON['x']
        self.y = itemJSON['y']
        self.itemType = itemType
        self.implicitMods = []
        self.explicitMods = []
        self.rawJSON = itemJSON

        for mod in itemJSON.get('implicitMods', []):
            self.implicitMods.append(parse(mod))
        for mod in itemJSON.get('explicitMods', []):
            self.explicitMods.append(parse(mod))

        self.desiredStats = itemData[self.itemType]

    def eval_value(self):
        self.evaluations = []
        for buildType in self.desiredStats:
            totalScore = 0
            scoredMods = []
            for i, mod in enumerate(self.explicitMods):
                prettyName = self.rawJSON['explicitMods'][i]
                modKey = mod[1].lower()
                if modKey in self.desiredStats[buildType]:
                    modValue = (float(mod[0]) / float(self.desiredStats[buildType][modKey]['max'])) * 100
                    if modValue < 50:
                        scoredMods.append((prettyName, -30))
                        totalScore = totalScore - 30
                    else:
                        scoredMods.append((prettyName, modValue))
                        totalScore = totalScore + modValue
                else:
                    scoredMods.append((prettyName, -15))
                    totalScore = totalScore - 15
            self.evaluations.append({"totalScore": totalScore, "scoredMods": scoredMods})
        self.evaluations = sorted(self.evaluations, key=lambda k: k['totalScore'], reverse=True)
