import React from 'react'
import ReactDOM from 'react-dom'
import { CookiesProvider } from 'react-cookie';

import Container from './components/container/Container.jsx'

export default class App extends React.Component {

    render() {
        return (
            <CookiesProvider>
                <Container />
            </CookiesProvider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))
