import React from 'react'
import ReactDOM from 'react-dom'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'

import Login from '../login/Login.jsx'
import Header from '../header/Header.jsx'

import styles from './style.css'

class Container extends React.Component {

    propTypes: {
        cookies: Cookies.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        const { cookies } = this.props;

        this.state = {
            acct: cookies.get('acct') || "yrdy",
            sessid: cookies.get('sessid') || ""
        }
    }

    render() {
        return (
            <div className="page-container">
                { this.state.acct.length > 0 && this.state.sessid.length > 0
                    ? <Header acct={ this.state.acct } sessid={ this.state.sessid } />
                    : <Login acct={ this.state.acct } sessid={ this.state.sessid }  /> }
            </div>
        )
    }
}

export default withCookies(Container);
