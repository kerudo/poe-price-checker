import React from 'react'
import ReactDOM from 'react-dom'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'

import styles from './style.css'

class Login extends React.Component {

    propTypes: {
        cookies: Cookies.isRequired
    }

    constructor(props) {
        super(props);
        // this.state = {};
        this.state = {
            acct: props.acct,
            sessid: props.sessid
        };

        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    login (event) {
        const { cookies } = this.props;
        event.preventDefault();
        console.log("Login was clicked");
        cookies.set('acct', this.state.acct, { path: '/' });
        cookies.set('sessid', this.state.sessid, { path: '/' });
    }

    render() {
        return (
            <div className="login-area">
                <label htmlFor="acct">Account Name</label>
                <input type="text" name="acct" className="form-control" value={this.state.acct} onChange={this.handleChange}></input>
                <label htmlFor="sessid">POESESSID</label>
                <input type="text" name="sessid" className="form-control" value={this.state.sessid} onChange={this.handleChange}></input>
                <a href="#" className="sessid-info">How to get your POESESSID</a>
                <a href="#" className="btn btn-block btn-lg btn-primary" onClick={this.login}>Login</a>
            </div>
        )
    }
}

export default withCookies(Login);
